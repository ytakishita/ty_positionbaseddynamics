using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FullPBD : MonoBehaviour
{

	//	a, 位置 x
	//	b, 速度 v
	//	c, 重み m

	public int PointNum;
	public int solverIterations;

	private Vector3[] x;
	private Vector3[] p;
	private Vector3[] v;
	private float[] m;
	private float[] w;

	public Vector3 g;
	//gravity
	public float dT;
	//delta time


	//------------サンプル用の位置を使うか------------
	public bool useSamplePosition = true;
	public GameObject prefab;
	Dictionary<int, GameObject> objects;


	//------------ラインレンダラー------------
	public bool useLineRenderer = true;
	LineRenderer lineRenderer;
	private Vector3[] linePoints = new Vector3[10];

	//------------メッシュ付きプレハブの入力------------

	public GameObject MeshPrefab;
	public Mesh Mesh;
	public bool isAttachedMesh = false;


	//------------ひれの形状------------

	/*
	private bool[] isBase = new bool[] {
	true, false, false, false
};
*/
//------------メッシュの生成------------
public bool makeMesh = true;
public int pointNumW = 10;
public int pointNumH = 10;
//Vector3[,] meshPoints;
//Mesh mesh = new Mesh ();
List<Vector3> verticeslist = new List<Vector3> ();
List<int> triangleslist = new List<int> ();

[SerializeField]
private Vector3[] vertices;
[SerializeField]
private int[] triangles;

//------------DistanceConstraint初期値設定------------

//	・[濃度 a cardinality] nj個,　制約条件の適合に用いられる頂点の数
//	・[関数 a function] Cj : R^3nj →R,  計算に使用される関数
//	・[インデックス a set of indices] {i1,...inj }, ik ∈ [1,...N], 取り扱う頂点のインデックス
//	・[硬さ係数 a stiffness parameter] kj ∈ [0...1] and,  0~1の値を取る硬さ係数
//	・[制約条件のタイプ a type of either equality or inequality.],  C=0（理想位置は条件に確実に一致している）となるべきか、C>=0（理想位置は一定以上の条件を満たしている）となるべきか。

//int nj = 2;
//void C();
float k = 1.0f;
//bool ifEquality = true;

Vector3 standardLength = new Vector3 (2.0f, 1.0f, 0.0f);
//float d = standardLength.magnitude;

//------------BendingConstraint初期値設定------------

//float k_Bending = 1.0f;
float standardAngle = 120.0f;

//------------Unity------------




// Use this for initialization ******************************************************************************
void Start ()
{

	solverIterations = 3; 		 //solver iterations


	//------------サンプルのポジション設定------------
	if (useSamplePosition) {
		PointNum = 4;  				 //Point Number

		// position
		x = new Vector3[] {
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (2.0f, 1.0f, 0.0f),
			new Vector3 (4.0f, 0.0f, 0.0f),
			new Vector3 (6.0f, -1.0f, 0.0f)
		};

		// potision
		p = new Vector3[] {
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
		};

		// velocity
		v = new Vector3[] {
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f)
		};

		// mass
		m = new float[] {
			100.0f, 1.0f, 1.0f, 1.0f
		};

		// weight
		w = new float[4];

		if (useLineRenderer) {

			lineRenderer = GetComponent<LineRenderer> ();

		}

	}

	else if (makeMesh) {

		Mesh mesh = new Mesh ();

		// 名前
		mesh.name = "meshGround";
		// 大きさ
		transform.localScale += new Vector3(5, 0, 5);

		//Vector3[] array;

		for (int y = 0; y < pointNumH; y++) {
			for (int _x = 0; _x < pointNumW; _x++) {

				Vector3 tmp = new Vector3 (_x - pointNumW / 2f, pointNumH / 2f, -y);
				verticeslist.Add (tmp);
			}
		}
		vertices = verticeslist.ToArray();

		for (int y = 0; y < pointNumH - 1; y++) {
			for (int _x = 0; _x < pointNumW - 1; _x++) {

				triangleslist.Add (pointNumH * y + _x);
				triangleslist.Add (pointNumH * y + _x + 1);
				triangleslist.Add (pointNumH * (y + 1) + _x);
			}
		}
		for (int y = 1; y < pointNumH; y++) {
			for (int _x = 1; _x < pointNumW; _x++) {

				triangleslist.Add (pointNumH * y + _x);
				triangleslist.Add (pointNumH * y + _x - 1);
				triangleslist.Add (pointNumH * (y - 1) + _x);
			}
		}


		triangles = triangleslist.ToArray();

		mesh.vertices = vertices;
		mesh.triangles = triangles;
		// 法線とバウンディングボックスを再計算
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshCollider>().sharedMesh = mesh;

		// カラーコード指定　※Unity5.3以降対応
		Color color;
		ColorUtility.TryParseHtmlString("#228B22", out color);

		// Material取得のためにRendererコンポーネントを取得
		Renderer rend = GetComponent<MeshRenderer>();
		rend.GetComponent<MeshRenderer>().material.color = color;



		PointNum = pointNumH * pointNumW;
		x = vertices;
		p = vertices;
		v = new Vector3[PointNum];
		m = Enumerable.Repeat<float>(1.0f, PointNum).ToArray();
		w = new float[PointNum];



	}


	//------------メッシュ設定------------
	else if (isAttachedMesh) {
		Mesh = GetComponent<MeshFilter> ().mesh;
		//Mesh = MeshPrefab.MeshFilter.mesh;

		Vector3[] vertices = Mesh.vertices;
		//Vector3[] normals = Mesh.normals;

		//初期設定
		PointNum = vertices.Length;
		x = vertices;
		p = vertices;
		v = new Vector3[PointNum];
		m = Enumerable.Repeat<float> (1.0f, PointNum).ToArray ();
		w = new float[PointNum];

		/*
		for (int i = 0; i < vertices.Length; i++) {

		}*/
	}


	//------------PBD初期値設定------------

	g = Physics.gravity;
	for (int i = 0; i < PointNum; i++) {
		w [i] = 1.0f / m [i];
	}


	//------------Unityオブジェクト設定------------
	if (useSamplePosition) {
		objects = new Dictionary<int, GameObject> ();

		for (int i = 0; i < PointNum; i++) {
			var obj = Instantiate (prefab);
			//obj.transform.SetParent(transform);
			obj.transform.position = x [i];
			objects.Add (i, obj);
		}
	}

}

// Update is called once per frame ******************************************************************************
void Update ()
{

	dT = Time.deltaTime;

	//------------PBD計算------------

	//		(4)ーループ始め
	//		→Update自体がループ

	//		(5)全ての頂点の速度に外力を加える
	//		→やらない
	for (int i = 0; i < PointNum; i++) {
		//v[i] = v[i] + dT * w[i] * f_ext[i];
		v [i] += g * dT;
	}

	//		(6)速度に減衰計算を行う
	//		→やらない

	//		(7)速度と経過時間から理想位置を算出
	for (int i = 0; i < PointNum; i++) {
		p [i] = x [i] + dT * v [i];
	}

	//		(8)衝突の制約条件を加えて理想位置を更新
	//		→やらない
	p [0] = x [0];

	//		(9)ーループ始め〜繰り返し回数だけ施行
	for (int solver_i = 0; solver_i < solverIterations; solver_i++) {

		//		(10)制約条件を計算
		//projectConstraint(C, p);
		//[距離制約 distance constraint]

		if (useSamplePosition) {
			distanceConstratint (0, 1, solver_i, 0f);
			distanceConstratint (1, 2, solver_i, 0f);
			distanceConstratint (2, 3, solver_i, 0f);
			bendingConstraint (0, 1, 2, solver_i);
			bendingConstraint (1, 2, 3, solver_i);
		} else if (makeMesh) {
			/*
			for (int j = 0; j < pointNumH; j++) {
			for (int i = 0; i < pointNumW; i++) {

			distanceConstratint (0, 1, solver_i);
		}
		}*/
		for (int i = 0; i < (pointNumH - 1) * (pointNumW - 1); i++) {

			int tmp = i * 3;
			distanceConstratint (triangleslist[tmp], triangleslist[tmp+1], solver_i, 1f);
			distanceConstratint (triangleslist[tmp], triangleslist[tmp+2], solver_i, 1f);
			distanceConstratint (triangleslist[tmp+2], triangleslist[tmp+1], solver_i, 1f);
		}
	}


	//		(11)ーループ終わり
}

//		(12)ーループ始め〜全ての頂点に対して
for (int i = 0; i < PointNum; i++) {
	//		(13)理想位置と現在位置の差分から、速度を計算
	v [i] = (p [i] - x [i]) / dT;

	//		(14)理想位置を現在位置に代入
	x [i] = p [i];

	//		(15)ーループ終わり
}


//		(16)摩擦と反発係数に応じて速度を更新
//		→やらない
//velocityUpdate(v);

//		(17)ーループ終わり


//------------Unityのオブジェクトに反映------------
if (useSamplePosition) {
	for (int i = 0; i < PointNum; i++) {
		objects [i].transform.position = x [i];
	}
} else if (isAttachedMesh) {
	Mesh.vertices = x;

	if (useLineRenderer) {

		linePoints = p;
		lineRenderer.SetPositions (linePoints);
	}

} else if (makeMesh) {
	Mesh Mesh = GetComponent<MeshFilter> ().mesh;
	//Mesh.Clear ();
	Mesh.vertices = x;
	//Mesh = mesh;

}

}

//[距離制約 distance constraint] ******************************************************************************
void distanceConstratint (int n1, int n2, int ns, float D)
{


	// p1, p2, w1, w2, d

	Vector3 p1 = p [n1];
	Vector3 p2 = p [n2];

	float w1 = w [n1];
	float w2 = w [n2];

	float d;
	if (D != 0f) {
		d = standardLength.magnitude;
	} else {
		d = D;
	}




	float wSum = w1 + w2;
	float d_p12 = (p1 - p2).magnitude;
	float dSub = d_p12 - d;

	Vector3 dp1 = -1 * (w1 / wSum) * dSub * ((p1 - p2) / d_p12);
	Vector3 dp2 = 1 * (w2 / wSum) * dSub * ((p1 - p2) / d_p12);

	float t = _k (ns) * dT;

	p [n1] = p1 + t * dp1;
	p [n2] = p2 + t * dp2;

	//		p [n1] = p1;
	//		p [n2] = p2;

}

void bendingConstraint (int n1, int n2, int n3, int ns)
{

	//p1, p2, w1, w2, phi

	float phi = standardAngle;

	Vector3 p1 = p [n1];
	Vector3 p2 = p [n2];
	Vector3 p3 = p [n3];

	Vector3 edge1 = p1 - p2;
	Vector3 edge2 = p3 - p2;

	float angle = Vector3.Angle (edge1, edge2);

	if (angle > phi) {
		return;
	}
	//float aSub = angle - phi;
	Vector3 rotAxis = Vector3.Cross (edge1, edge2);
	Quaternion rot = Quaternion.AngleAxis (angle, rotAxis);
	Matrix4x4	rotMat = Matrix4x4.TRS (p2, rot, Vector3.one);
	Vector3 dp3 = rotMat.MultiplyPoint3x4 (edge2) + p2;

	float t = _k (ns) * dT;

	p [n3] = p3 + t * dp3;

	Debug.Log (n2 + " : " + angle);

}


float _k (int ns)
{
	return 1.0f - Mathf.Pow (1.0f - k, 1.0f / ns);
}

}
