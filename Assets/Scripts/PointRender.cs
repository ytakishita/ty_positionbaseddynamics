using UnityEngine;
using System.Collections.Generic;

namespace PBDSample{
	class PointRender : MonoBehaviour
	{
		public GameObject prefab = null;
		Dictionary<Particle, GameObject> objects;

		public void Start()
		{
			// GameObjectを生産
			objects = new Dictionary<Particle, GameObject>();
			foreach (var particle in GetComponent<Main_PBDCloth>()._particles)
			{
				var obj = Instantiate(prefab);
				obj.transform.SetParent(transform);
				objects.Add(particle, obj);
			}
		}

		void Update()
		{
			// GameObjectの位置を更新する
			foreach (var obj in objects)
			{
				obj.Value.transform.position = obj.Key.p0;
			}
		}
	}
}
