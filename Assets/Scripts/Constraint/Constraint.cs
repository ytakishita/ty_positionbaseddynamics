
﻿using UnityEngine;
using System.Collections;

namespace PBDSample{
public class Constraint : MonoBehaviour {

	public int n = 0;
	public int[] indices;
	public Particle[] positions;
	public float k = 1.0f;
	public bool isEquality = true;


	public Constraint(params int[] indices) {
		this.n = indices.Length;
		this.indices = indices;
		this.positions = new Particle[n];
	}

	public float eval(int iterations) {
		return 0;
	}

	public void setPosition(int i, Particle p) {
		positions[i] = p;
	}

	public Particle getPosition(int i) {
		return positions[i];
	}

	protected float _k(int ns) {
		return 1.0f - Mathf.Pow(1.0f - k, 1.0f / ns);
	}

}
}
