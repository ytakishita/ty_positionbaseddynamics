using UnityEngine;
using System.Collections;
/*
namespace PBDSample{
  class BendConstraint : Constraint {
    class Result {
      public PVector n0, n1;
      public float d, phi;
    }
    public float phi0;
    private Result _res;
    PVector _cpp12, _cpp13, _cpn11, _cnp01, _cpn10, _cnp11, _cpn21, _cnp02, _cpn30, _cnp13;
    public BendConstraint(int i0, int i1, int i2, int i3, Particle pa0, Particle pa1, Particle pa2, Particle pa3) {
      super(i0, i1, i2, i3);
      _res = new Result();
      _cpp12 = new PVector();
      _cpp13 = new PVector();
      _cpn11 = new PVector();
      _cnp01 = new PVector();
      _cpn10 = new PVector();
      _cnp11 = new PVector();
      _cpn21 = new PVector();
      _cnp02 = new PVector();
      _cpn30 = new PVector();
      _cnp13 = new PVector();
      // calc phi0
      Point p0 = pa0.p0, p1 = pa1.p0, p2 = pa2.p0, p3 = pa3.p0;
      phi0 = _phi(p0, p1, p2, p3).phi;
      println("phi9=" + phi0);
    }
    public float eval(int interations) {
      Point p0 = positions[0].p1;
      Point p1 = positions[1].p1;
      Point p2 = positions[2].p1;
      Point p3 = positions[3].p1;

      Result res = _phi(p0, p1, p2, p3);
      float d = res.d;
      float c = res.phi - phi0;

      if(!isEquality && c >= 1.0f) {
        return c;
      }

      PVector n0 = _res.n0, n1 = _res.n1;

      PVector.cross(p1, p2, _cpp12);
      PVector.cross(p1, p3, _cpp13);
      PVector.cross(p1, n1, _cpn11);
      PVector.cross(n0, p1, _cnp01);
      PVector.cross(p1, n0, _cpn10);
      PVector.cross(n1, p1, _cnp11);
      PVector.cross(p2, n1, _cpn21);
      PVector.cross(n0, p2, _cnp02);
      PVector.cross(p3, n0, _cpn30);
      PVector.cross(n1, p3, _cnp13);

      float d12 = _cpp12.mag();
      float d13 = _cpp13.mag();

      PVector q2 = PVector.div(PVector.add(_cpn11, PVector.mult(_cnp01, d)), d12);
      PVector q3 = PVector.div(PVector.add(_cpn10, PVector.mult(_cnp11, d)), d13);
      PVector q20 = PVector.div(PVector.add(_cpn21, PVector.mult(_cnp02, d)), d12);
      PVector q21 = PVector.div(PVector.add(_cpn30, PVector.mult(_cnp13, d)), d13);
      PVector q1 = PVector.add(PVector.mult(q20, -1), PVector.mult(q21, -1));
      PVector q0 = PVector.sub(PVector.sub(PVector.mult(q1, -1), q2), q3);

      // TODO: calc delta p_i

      return c;
    }
    Result _phi(PVector p0, PVector p1, PVector p2, PVector p3) {
      PVector n0 = _res.n0 = _normal(p0, p1, p2);
      PVector n1 = _res.n1 = _normal(p0, p1, p3);
      float d = _res.d = PVector.dot(n0, n1);
      _res.phi = acos(d);
      return _res;
    }
    PVector _normal(PVector p0, PVector p1, PVector p2) {
      PVector c0 = new PVector();
      PVector.cross(PVector.sub(p1, p0), PVector.sub(p2, p0), c0);
      float d0 = c0.mag();
      if(d0 == 0) {
        return new PVector(0, 0, 0);
      }
      return PVector.div(c0, d0);
    }
  }
}*/
