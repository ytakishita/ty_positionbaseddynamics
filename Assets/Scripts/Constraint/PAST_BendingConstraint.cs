﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class PAST_BendingConstraint: Constraint{

		public float phiRad;
		public float ToleranceAngle = 30.0f;
		private float angle = -1.0f;
		private Vector3 rotAxis = Vector3.zero;

		public struct BendPairs{
			int p1, p2, p3;

			public BendPairs(int _p1, int _p2, int _p3){
				p1 = _p1;
				p2 = _p2;
				p3 = _p3;
			}
		}
		static public List<BendPairs> finPointList   = new List<BendPairs>();


		public PAST_BendingConstraint(Point _P1, Point _P2, Point _P3, float _PhiDeg){

			phiRad = _PhiDeg * Mathf.Deg2Rad;

			p = new Point[3];
			p[0] = _P1;
			p[1] = _P2;
			p[2] = _P3;

		}
		public void run(int ns)
		// (int n1, int n2, int n3, int ns)
		{
			Debug.Log("[ " + this.GetType().FullName + " ] Points: " + p[0].index +  ", " + p[1].index + ", " + p[2].index + " Phi: " + phiRad* Mathf.Rad2Deg);

			float dT = Time.deltaTime;
			float t = _k (ns) * dT;
			//p1, p2, w1, w2, phi
			//float phi = 120f;//standardAngle;

			Vector3 _p1 = p[0].p1;
			Vector3 _p2 = p[1].p1;
			Vector3 _p3 = p[2].p1;

			Vector3 p1 = _p2;
			Vector3 p2 = _p2;
			Vector3 p3 = _p1;
			Vector3 p4 = _p3;

			float w1 = p[0].w;
			float w2 = p[1].w;
			float w3 = p[2].w;

			float[] w = new float[] {
				w2, w2, w1, w3
			};

			float wSum = w1 + w2 + w3;
			//*Debug.Log("[ " + this.GetType().FullName + " ]wSum: " + wSum);

			Vector3 edge1 = _p1 - _p2;
			Vector3 edge2 = _p3 - _p2;
			//*Debug.Log("[ " + this.GetType().FullName + " ]edge: " + edge1 + ", " + edge2 );

			//float aSub = angle - phi;
			//処理の手順
			//真ん中の頂点をp2とする
			//p2から出ている辺2つをedge1, edge2とする
			//edge1とedge2がなす角度がangle
			//edge1とedge2がなす平面の法線がrotAxis
			//axis の周りを angle 度回転する回転を作成
			//：Quaternion AngleAxis(float angle, Vector3 axis);
			//移動、回転やスケーリングする行列を作成
			//返される行列は場所について、位置をpos、回転の向きをq、スケールをsで修正
			//：Matrix4x4 TRS(Vector3 pos, Quaternion q, Vector3 s);
			//p3に位置する頂点を回転して移動させた位置ベクトルをdp3に返す

			//if(angle < 0.0f){
			angle = Vector3.Angle (edge1, edge2);
			//}
			Debug.Log("[ " + this.GetType().FullName + " ]angle: " + angle);

			if (angle > 120.0f/* phi*Mathf.Rad2Deg*/) {
				//return;
			}
			float dAngle = 0.0f;
			if(angle - phiRad * Mathf.Rad2Deg > ToleranceAngle){
				dAngle = angle - phiRad * Mathf.Rad2Deg;
			}

			//Vector3
			if(rotAxis == Vector3.zero){
				rotAxis = (Vector3.Cross (edge1, edge2)).normalized;
			}
			//Quaternion rot = Quaternion.AngleAxis (angle, rotAxis);
			Quaternion rot = Quaternion.AngleAxis (dAngle, rotAxis);
			Matrix4x4	rotMat = Matrix4x4.TRS (_p2, rot, Vector3.one);
			Vector3 dp3 = rotMat.MultiplyPoint3x4 (edge2) + _p2;

			p[2].p1 = _p3 + t * dp3;

			// p1 = _p2 + rotAxis * 0.5f;
			// p2 = _p2 - rotAxis * 0.5f;

			//*********Bending
			Debug.Log("[ " + this.GetType().FullName + " ]rotAxis: " + rotAxis);

			Vector3 n1 = (Vector3.Cross(rotAxis, edge1)).normalized;
			Vector3 n2 = (Vector3.Cross(rotAxis, edge2)).normalized;
			float d = Vector3.Dot(n1, n2);

			//*Debug.Log("[ " + this.GetType().FullName + " ]n1, n2: " + n1 + ", " + n2);
			//*Debug.Log("[ " + this.GetType().FullName + " ]d: " + d);

			Vector3[] q = new Vector3[]{
				Vector3.zero,
				Vector3.zero,
				Vector3.zero,
				Vector3.zero
			};
			//Vector3 q3;
			q[3-1] = (Vector3.Cross(p2, n2) + Vector3.Cross(n1, p2) * d) / (Vector3.Cross(p2, p3)).magnitude;

			//*Debug.Log("[ " + this.GetType().FullName + " ]q3: " + q[2]);

			// Vector3 q4;
			q[4-1] = (Vector3.Cross(p2, n1) + Vector3.Cross(n2, p2) * d) / (Vector3.Cross(p2, p4)).magnitude;
			//*Debug.Log("[ " + this.GetType().FullName + " ]q4: " + q[3]);

			Vector3 t1 = (Vector3.Cross(p3, n2) + Vector3.Cross(n1, p3) * d) / (Vector3.Cross(p2, p3)).magnitude;
			Vector3 t2 = (Vector3.Cross(p4, n1) + Vector3.Cross(n2, p4) * d) / (Vector3.Cross(p2, p4)).magnitude;

			//*Debug.Log("[ " + this.GetType().FullName + " ]t1: " + t1);
			//*Debug.Log("[ " + this.GetType().FullName + " ]t2: " + t2);

			//Vector3 q2;
			q[2-1] = -t1 -t2;
			//Vector3 q1;
			q[1-1] = -q[2-1] -q[3-1] -q[4-1];
			//*Debug.Log("[ " + this.GetType().FullName + " ]q2: " + q[1]);
			//*Debug.Log("[ " + this.GetType().FullName + " ]q1: " + q[0]);


			//Vector3[4] dp;
			Vector3[] dp = new Vector3[]{
				Vector3.zero,
				Vector3.zero,
				Vector3.zero,
				Vector3.zero
			};
			float denom = 0.0f;

			for(int i=0; i<4; i++){
				denom += w[i] * q[i].magnitude;
			}
			//*Debug.Log("[ " + this.GetType().FullName + " ]denom: " + denom);

			for(int i=0; i<4 ; i++){
				dp[i] = ((-w[i] * Mathf.Sqrt(1-d*d) * (Mathf.Acos(d) - phiRad )) / denom ) * q[i];

				//Debug.Log("[ " + this.GetType().FullName + " ]i: " + i);
				//Debug.Log("[ " + this.GetType().FullName + " ]p: " + p[i]);
				//Debug.Log("[ " + this.GetType().FullName + " ]q: " + q[i]);
				//*Debug.Log("[ " + this.GetType().FullName + " ]Delta P[" + i + "]: " + dp[i]);
			}

			//*********

			//float t = _k (ns) * dT;

			// p[0].p1 = p3 + t * dp[3-1];
			// p[1].p1 = p2 + t * dp[2-1];// + rotAxis * 0.5f;
			// p[2].p1 = p4 + t * dp[4-1];

			for(int i=0; i<3; i++){
				Debug.Log("[ " + this.GetType().FullName + " ]p[" + i + "]: " + p[i].p1);
			}

			//sDebug.Log (p[2].index + " : " + angle);

		}

		//PBDの計算用
		public float _k (int ns)
		{
			return 1.0f - Mathf.Pow (1.0f - k, 1.0f / ns);
		}

		static public bool Add(int _p1, int _p2, int _p3){

			BendPairs pair = new BendPairs(_p1, _p2, _p3);

			if(finPointList.Contains(pair)){
				Debug.Log("すでに含まれています( " + _p1 + ", " + _p2 + " )");
				return false;
			}else {
				Debug.Log("ペアを追加しました( " + _p1 + ", " + _p2 + " )");
				finPointList.Add(pair);
				return true;
			}

		}


	}
}
