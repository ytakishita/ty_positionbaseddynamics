﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PBDSample{
public class Main_PBDCloth : MonoBehaviour {

	private Mesh objMesh;
	private Vector3[] objVertices;
	private int vertexCount;


	public List<Particle> _particles;
	public List<Constraint> _constraints;
	public List<Vector3> _forces; // external force

//	int _solverIterations = 4;
//	int _updateIterations = 1;


	void Awake () {

		int i = 0;

		// pointを生産する
		_particles = new List<Particle>();

		foreach (var x in Enumerable.Range(-2, 4))
		{
			foreach (var y in Enumerable.Range(-2, 8))
			{
				foreach (var z in Enumerable.Range(-2, 4))
				{
					var Particle = new Particle();
					Particle.p0 = Particle.p1 = new Vector3(x + y * 0.1f, y, z + y * 0.1f) * 1.0f;
					_particles.Add(Particle);

					Debug_Particle (_particles[i], i++);
				}
			}
		}
	}



	// Use this for initialization
	void Start () {

		/*
		objMesh		 	 = GetComponent<MeshFilter>().mesh;
		objVertices		 = objMesh.vertices;
		vertexCount		 = objMesh.vertexCount;

		// for all vertices i do
		// initialize xi =x0 i, vi =v0 i, wi =1/mi
		for (int i = 0; i < vertexCount; i++) {

			Vector3 Pos = objVertices [i];
			Particle particle = new Particle();

			particle.set(Pos, Pos, Vector3.zero);
			_particles.Add (particle);

			//Debug_Particle (_particles[i], i);
		}

		//_constraints = generateConstraints ();
		DebugStr ("パーティクルのセット終了");
		*/

	}


	void FixedUpdate () {

		int i_ = 0;

		foreach (var particle in _particles) {

			//for all vertices i do vi ← vi + ∆t wi fext(xi)
			particle.v += Physics.gravity * Time.deltaTime;  //DebugStr ("現在位置と理想位置の間にたどり着くまでの速度");

			//for all vertices i do pi ← xi + ∆tvi
			particle.p1 = particle.p0 + particle.v * Time.deltaTime;
		}
		//Debug.Log (_particles.Count);
		//DebugStr ("理想位置の更新終了");


		//loop solverIteration times
		//project Constraints
		foreach (var i in Enumerable.Range(0, 3))
		{
			Debug.Log("繰り返し回数 " + i);
			foreach (var particle in _particles)
			{

				// 位置を範囲内に制限する Constraint
				var position = particle.p1;
				position.x = Mathf.Clamp(position.x, -5, 5);
				position.y = Mathf.Clamp(position.y, -5, 5);
				position.z = Mathf.Clamp(position.z, -5, 5);

				particle.p1 = position;

				// point同士が衝突していたら離す DistanceConstraint
				/*
				foreach (var point2 in _particles)
				{
					if (point == point2) continue;
					var vec = point2.p1 - point.p1;
					if (vec.magnitude < 1)
					{
						point.p1 -= vec.normalized * (1 - vec.magnitude) / 2;
						point2.p1 += vec.normalized * (1 - vec.magnitude) / 2;
					}
				}*/

			}
		}//end loop

		//for all vertices i do
		foreach (var particle in _particles) {

			i_++;  //Debug_Particle (particle, i++);

			//vi ←(pi−xi)/∆t
			particle.v = (particle.p1 - particle.p0) / Time.deltaTime;

			// 理想位置を更新する
			//xi ← pi
			particle.p0 = particle.p1;
			//particle.p1 += particle.v * Time.deltaTime;

		}

		//bvelocityUpdate(_particles);

	}

	// Update is called once per frame
	void Update(){
		/*
		int i = 0;
		foreach (var particles in _particles)
		{
			objVertices[i++] = particles.p1;

		}
		//		Debug.Log ("配列に入れ直す際のパーティクルの数:" + i);
		//		DebugStr ("パーティクルの値を配列に入れ直す");

		objMesh.vertices = objVertices;
		//		DebugStr ("メッシュへの代入");
*/
	}


	//List<Constraint> generateConstraints (List<Particle> particles){


		/* DistanceConstraint
		int step = segX + 1;
		float dw = w / segX, dh = h / segY, dr = sqrt(dw * dw + dh * dh);
		for(int iy = 0; iy < segY; ++ iy) {
			for(int ix = 0; ix < segX; ++ ix) {
				int i = iy * step + ix;
				int i0 = i, i1 = i + 1, i2 = i + step, i3 = i2 + 1;
				_constraints.Add(new DistanceConstraint(i3, i2, dw));
				_constraints.Add(new DistanceConstraint(i2, i0, dh));
				_constraints.Add(new DistanceConstraint(i0, i3, dr));
				_constraints.Add(new DistanceConstraint(i3, i0, dr));
				_constraints.Add(new DistanceConstraint(i0, i1, dw));
				_constraints.Add(new DistanceConstraint(i1, i3, dh));
				_constraints.Add(new DistanceConstraint(i2, i1, dr));
			}
		}


	}*/



	//------------デバッグ関数------------------------------------------------------

	void Debug_Particle(Particle P){

		Debug.Log ("CurrentPos: " + P.p0 + "\n");
		Debug.Log ("NextPos   : " + P.p1 + "\n");
		Debug.Log ("Velocity  : " + P.v + "\n");
	}

	void Debug_Particle(Particle P, int i){

		Debug.Log (i + " CurrentPos: " + P.p0 + "\n");
		Debug.Log (i + " NextPos   : " + P.p1 + "\n");
		Debug.Log (i + " Velocity  : " + P.v + "\n");

	}

	void DebugVec(Vector3 vec) {
		Debug.Log ( vec + "\n");
	}

	void DebugStr(string str){

		Debug.Log ( str + "\n");
	}

}
}
