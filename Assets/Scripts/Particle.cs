﻿using UnityEngine;
using System.Collections;

namespace PBDSample{
public class Particle : MonoBehaviour {

	public Vector3 p0; // current
	public Vector3 p1; // next
	public Vector3 v;
	public float mass = 1.0f;

	public Particle() {

		Vector3 zero = Vector3.zero;
		p0 = zero;
		p1 = zero;
		v = zero;
		//this(zero, zero, zero);
	}

	public Particle(Vector3 Pos, Vector3 nextPos) {

		Vector3 zero = Vector3.zero;
		p0 = Pos;
		p1 = nextPos;
		v = zero;
		//this(Pos, nextPos, zero);
	}

	public Particle(Vector3 Pos, Vector3 nextPos, Vector3 verocity) {
		p0 = Pos;
		p1 = nextPos;
		v = verocity;
	}

	public void set(Vector3 Pos, Vector3 nextPos, Vector3 verocity) {

		setPosition(Pos);
		setNextPosition(nextPos);
		setVelocity(verocity);

	}

	public void setPosition(Vector3 Pos) {
		p0 = Pos;
	}
	public void setNextPosition(Vector3 nextPos) {
		p1 = nextPos;
	}
	public void setVelocity(Vector3 velocity) {
		v = velocity;
	}
}
}
