﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]

public class MakeMesh : MonoBehaviour {

	public int pointNumW = 10;
	public int pointNumH = 10;

	[SerializeField]
	private Vector3[] vertices/* =
	{
		new Vector3(-1, 0, -10),   // 左下の頂点、頂点インデックス 0 番
		new Vector3(1, 0, -1),    // 右下の頂点、頂点インデックス 1 番
		new Vector3(1, 0, 1),     // 右上の頂点、頂点インデックス 2 番
		new Vector3(-1, 0, 1)     // 左上の頂点、頂点インデックス 3 番
	}*/;

	[SerializeField]
	private Vector2[] UV/*=
	{
		new Vector2(0, 0),      // 左下から
		new Vector2(1, 0),
		new Vector2(0, 1),
		new Vector2(1, 1)       // 右上へ
	}*/;

	// 頂点インデックス
	[SerializeField]
	private int[] triangles /*=
	{
		0, 2, 1,    // 表は時計回り。左下 → 右上 → 右下
		0, 3, 2     // 表は時計回り。左下 → 左上 → 右上
	}*/;


	//Mesh mesh = new Mesh();

	// Use this for initialization
	void Start () {

		Mesh mesh = new Mesh();

		//Vector3[] array;
		List<Vector3> verticeslist = new List<Vector3>();
		for (int y = 0; y < pointNumH; y++) {
			for (int x = 0; x < pointNumW; x++) {

				Vector3 tmp = new Vector3 (x - pointNumW/2f, pointNumH/2f, -y);
				verticeslist.Add (tmp);
			}
		}

		vertices = verticeslist.ToArray();

		List<int> triangleslist = new List<int>();
		for (int y = 0; y < pointNumH-1; y++) {
			for (int x = 0; x < pointNumW-1; x++) {

				triangleslist.Add (pointNumH * y + x);
				triangleslist.Add (pointNumH * y + x + 1);
				triangleslist.Add (pointNumH * ( y + 1 ) + x);
			}
		}
		for (int y = 1; y < pointNumH; y++) {
			for (int x = 1; x < pointNumW; x++) {

				triangleslist.Add (pointNumH * y + x);
				triangleslist.Add (pointNumH * y + x - 1);
				triangleslist.Add (pointNumH * ( y - 1 ) + x);
			}
		}

		triangles = triangleslist.ToArray();

		Debug.Log (vertices.Length);
		Debug.Log (triangles.Length);

		mesh.vertices = vertices;
		mesh.triangles = triangles;

		// 法線とバウンディングボックスを再計算
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		// 名前
		mesh.name = "meshGround";
		// 大きさ
		transform.localScale += new Vector3(5, 0, 5);

		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshCollider>().sharedMesh = mesh;

		// カラーコード指定　※Unity5.3以降対応
		Color color;
		ColorUtility.TryParseHtmlString("#228B22", out color);

		// Material取得のためにRendererコンポーネントを取得
		Renderer rend = GetComponent<MeshRenderer>();
		rend.GetComponent<MeshRenderer>().material.color = color;

		/*
		// 頂点の指定
		mesh.vertices = new Vector3[] {
			new Vector3(-1, -1, 0),
			new Vector3(1, -1, 0),
			new Vector3(-1, 1, 0),
			new Vector3(1, 1, 0),
		};
		// UV座標の指定
		mesh.uv = new Vector2[] {
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(1, 1),
			new Vector2(0, 1),
		};
		// 頂点インデックスの指定
		mesh.triangles = new int[] {
			0, 1, 2,
			0, 2, 3,
		};

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		filter.sharedMesh = mesh;

		*/
	}

	// Update is called once per frame
	void Update () {

		/*
		Mesh mesh = new Mesh();

		Vector3[] array;
		List<Vector3> verticeslist = new List<Vector3>();
		for (int y = 0; y < pointNumH; y++) {
			for (int x = 0; x < pointNumW; x++) {

				Vector3 tmp = new Vector3 (x - pointNumW/2f, pointNumH/2f, -y);
				verticeslist.Add (tmp);
			}
		}

		vertices = verticeslist.ToArray();

		List<int> triangleslist = new List<int>();
		for (int y = 0; y < pointNumH-1; y++) {
			for (int x = 0; x < pointNumW-1; x++) {

				triangleslist.Add (pointNumH * y + x);
				triangleslist.Add (pointNumH * y + x + 1);
				triangleslist.Add (pointNumH * ( y + 1 ) + x);
			}
		}
		for (int y = 1; y < pointNumH; y++) {
			for (int x = 1; x < pointNumW; x++) {

				triangleslist.Add (pointNumH * y + x);
				triangleslist.Add (pointNumH * y + x - 1);
				triangleslist.Add (pointNumH * ( y - 1 ));
			}
		}

		triangles = triangleslist.ToArray();

		mesh.vertices = vertices;
		mesh.triangles = triangles;

		// 法線とバウンディングボックスを再計算
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();


		GetComponent<MeshFilter>().sharedMesh.Clear ();
		GetComponent<MeshCollider>().sharedMesh.Clear ();

		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshCollider>().sharedMesh = mesh;

*/
	}
}
