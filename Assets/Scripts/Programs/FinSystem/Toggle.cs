﻿using UnityEngine;
using System.Collections;

public class Toggle : MonoBehaviour {

	public enum WitchFinSet{
		BASE, RAY, MEMBERANCE, NONE
	}

	public static WitchFinSet SetType;

	// Use this for initialization
	void Start () {
		SetType = WitchFinSet.NONE;


	}

	// Update is called once per frame
	void Update () {

	}

	public void clickSetNoneToggle(){
		SetType = WitchFinSet.NONE;
		Debug.Log("NoneToggleが変更されました");
	}

	public void clickSetFinBaseToggle(){
		SetType = WitchFinSet.BASE;
		Debug.Log("BaseToggleが変更されました");
	}
	public void clickSetFinRayToggle(){
		SetType = WitchFinSet.RAY;
		Debug.Log("RayToggleが変更されました");
	}
	public void clickSetFinMemberanceToggle(){
		SetType = WitchFinSet.MEMBERANCE;
		Debug.Log("MemberanceToggleが変更されました");
	}
}
