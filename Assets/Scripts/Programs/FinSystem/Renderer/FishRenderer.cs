using UnityEngine;
using System.Collections;

namespace FinSystem{
	public class FishRenderer : MonoBehaviour {

		private Fin[] AllFin;
		private int kFinNum;
		public GameObject basePoint;
		public GameObject rayPoint;
		bool restart = false;
		FinController finCon;

		public GameObject[] allParticle;

		// Use this for initialization
		void Start () {

			Debug.Log("[ " + this.GetType().FullName + " ] StartCheckPoint: 1" );

			finCon = GetComponent<FinController>();
			restart = true;

		}

		// Update is called once per frame
		void Update () {

			if(restart)
			{

				allParticle = GameObject.FindGameObjectsWithTag("Point");
				foreach (GameObject obj in allParticle) {
					Destroy (obj);
				}

				AllFin = finCon.getAllFin();


				if(AllFin != null || AllFin.Length != 0)
				{
					kFinNum = AllFin.Length;
					for(int fin_i=0; fin_i<kFinNum; fin_i++)
					{
						Fin thisFin = AllFin[fin_i];


						////デバッグ----------------------------
						//Debug.Log("[ " + this.GetType().FullName + " ] "+"*****************************");
						//thisFin.LogAllFinPoint();
						//thisFin.LogAllFinPosition();
						////デバッグ----------------------------

						FinBase thisBase = thisFin.getFinBase();
						FinRay thisRays = thisFin.getFinRay();

						//基底の頂点の表示.
						Vector3[] baseList = thisBase.getPosition();
						for (int base_i=0; base_i < thisFin.getBaseNum(); base_i++){
							var obj = Instantiate(basePoint);
							Vector3 pos = baseList[base_i];
							obj.tag = "Point";
							obj.transform.position = pos;

						}
						//鰭条の頂点の表示.
						for(int rays_i=0; rays_i < thisRays.N; rays_i++){

							Vector3[] ray = thisRays.position[rays_i];
							int kRayNum = ray.Length;
							for(int i=0; i< kRayNum; i++){
								var obj = Instantiate(rayPoint);
								Vector3 pos = ray[i];
								obj.tag = "Point";
								obj.transform.position = pos;
							}
						}
					}
					//restart = false;
				}
			}

		}
	}
}
