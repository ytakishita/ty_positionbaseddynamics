using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{

	//static int logCount=0;

	public class Fish : MonoBehaviour {

		public enum FishModelType{
			Mesh, Point
		}

		public struct MeshFishModel{
			private MeshFilter Filter;
			private Mesh Mesh;
			private Point[] Points;
			private int[] Triangles;

			public void setFilter(MeshFilter _F){
				Filter = _F;
			}
			public void setMesh(){
				Mesh = Filter.mesh;
			}
			public void createPoints(){
				List<Point> list = new List<Point>();
				foreach(Vector3 v in this.Mesh.vertices){
					list.Add(new Point(v));
				}
				this.Points = list.ToArray();
			}

			public void setVertics(Vector3[] _V ){
				Mesh.vertices = _V;
			}
			public void setTriangles(int[] _T ){
				Mesh.triangles = _T;
			}
			public void getPoints(Point[] _P){
				_P = Points;
			}
			public void setPoints(Point[] _P){
				Points = _P;
			}
			public Vector3[] getVertices(){
				return Mesh.vertices;
			}

		}

		public struct PointFishModel{
			private Vector3[] Position;
			private Point[] Points;
		}

		public struct Comstraints{
			Constraint[] all_constraints;
			List<Constraint> constraints;
		}

		public FishModelType Type;

		private MeshFishModel 	 MeshModel;
		private PointFishModel	 PointModel;

		// Use this for initialization
		void Start () {
			Debug.Log("[ " + this.GetType().FullName + " ]  StartCheck" );

			switch(Type){
				case FishModelType.Mesh:
				MeshModel = new MeshFishModel();
				MeshModel.setFilter(GetComponent<MeshFilter>());
				MeshModel.setMesh();
				MeshModel.createPoints();
				break;

				case FishModelType.Point:
				PointModel = new PointFishModel();
				break;

			}
		}

		// Update is called once per frame
		void Update () {

		}
	}
}
