using UnityEngine;
using System.Collections;

namespace FinSystem{
	public class GenerateExtForce : MonoBehaviour {

		private FinController finCon;

		public bool back_forward;
		public Vector3 Direct;
		public Vector3 Force;
		public float Adjust;

		private LineRenderer LineRend;
		private Vector3[] linePoints;
		public Vector3 adjustPos;

		// Use this for initialization
		void Start () {

			finCon = GetComponent<FinController>();
			Force = Vector3.zero;


			LineRend = GetComponent<LineRenderer>();
			linePoints = new Vector3[]{Vector3.zero + adjustPos, Vector3.zero + adjustPos};
			LineRend.SetPositions(linePoints);


		}

		// Update is called once per frame
		void Update () {

			if( Input.GetKey( KeyCode.UpArrow ) ){

				if(back_forward){
					Direct += Vector3.up * Adjust;
					Force += Vector3.forward * Adjust;
				}
				else {
					Direct += Vector3.up * Adjust;
					Force += Vector3.up * Adjust;
				}
				Debug.Log("[ " + this.GetType().FullName + " ] Force: " + Force );

				displayLine();
				finCon.setExtForce(Force);
			}
			else if( Input.GetKey( KeyCode.DownArrow ) ){

				if(back_forward){
					Direct += Vector3.down * Adjust;
					Force += Vector3.back * Adjust;
				}
				else {
					Direct += Vector3.down * Adjust;
					Force += Vector3.down * Adjust;
				}
				Debug.Log("[ " + this.GetType().FullName + " ] Force: " + Force );

				displayLine();
				finCon.setExtForce(Force);
			}
			else if( Input.GetKey( KeyCode.RightArrow ) ){

				Direct += Vector3.right * Adjust;
				Force += Vector3.right * Adjust;
				Debug.Log("[ " + this.GetType().FullName + " ] Force: " + Force );
				displayLine();
				finCon.setExtForce(Force);
			}
			else if( Input.GetKey( KeyCode.LeftArrow ) ){

				Direct += Vector3.left * Adjust;
				Force += Vector3.left * Adjust;
				Debug.Log("[ " + this.GetType().FullName + " ] Force: " + Force );
				displayLine();
				finCon.setExtForce(Force);
			}
			else{

			}

			//Debug.Log("[ " + this.GetType().FullName + " ] Force: " + Force );

		}

		private void displayLine(){

			linePoints[0] = adjustPos;
			linePoints[1] = Direct * 0.1f + adjustPos;
			LineRend.SetPositions(linePoints);

		}
	}
}
