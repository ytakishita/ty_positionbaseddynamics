using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class CreateParticleListFromMesh : MonoBehaviour {

		public float deltaTime = 1.0f / 30.0f;
		Vector3[] all_particles;
		OverlapPoint[] _particles;
		List<Vector3> list;
		Mesh m;
		int[] _triangles;

		// Use this for initialization
		void Start () {
			m = GetComponent<MeshFilter>().mesh;
			all_particles = m.vertices;
			_triangles = m.triangles;
			InitParticleList();
		}

		void InitParticleList(){
			//全ての頂点をワールド座標に移動.
			for(int i = 0; i < all_particles.Length; i++) {
				all_particles[i] = transform.TransformPoint(all_particles[i]);

			}
			//particle頂点から、位置が重複するものを除く.
			list = new List<Vector3>(all_particles.Length);
			foreach(Vector3 p in all_particles){
				if (!list.Contains(p)) {
					list.Add(p);
				}
			}

			//重複を除いたListからparticleの配列を作成する.
			_particles = new OverlapPoint[list.Count];
			for(int i = 0; i < list.Count; i++) {
				_particles[i] = new OverlapPoint(list[i]);
			}

			//同じ座標の頂点があれば、同じparticleとして保存.
			for(int i = 0; i < _particles.Length; i++) {
				for(int j = 0; j < all_particles.Length; j++) {
					if(_particles[i].p0 == all_particles[j]) {
						_particles[i].sameList.Add(j);
					}
				}
			}
			//三角形を構成する頂点の中に重複するものがあったら、sameListの最初の値を代入する.
			for(int i = 0; i < _particles.Length; i++) {
				for(int j = 1; j < _particles[i].sameList.Count; j++) {
					for(int k = 0; k < _triangles.Length; k++) {
						if(_triangles[k] == _particles[i].sameList[j]) {
							_triangles[k] = _particles[i].sameList[0];
						}
					}
				}
			}
		}

		// Update is called once per frame
		void Update () {

		}
	}
}
