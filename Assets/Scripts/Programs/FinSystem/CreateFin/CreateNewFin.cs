using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public static class CreateNewFin{

		static public float adjust = 0.2f;

		//基底をつくる.
		public static Vector3[] createFinBase(Vector3 startPoint, Vector3 endPoint, ref int baseNum, int thikness)
		{
			List<Vector3> baseList = new List<Vector3>();

			//開始点と終了点が異なり、基底の数が1以上のとき.
			if(startPoint != endPoint || baseNum > 1){

				float space = 1f / (baseNum - 1);
				Vector3 d = endPoint - startPoint;
				Vector3 spaced = d * space;

				for (int i=0; i<baseNum; i++){
					Vector3 tmp = startPoint + spaced * i;
					baseList.Add(tmp);
				}
			}else{
				baseList.Add(startPoint);
				baseList.Add(endPoint);
				baseNum = 2;
			}
			//基底の頂点配列を返す.
			Vector3[] finBase = baseList.ToArray();
			return finBase;
		}


		//鰭条をつくる.
		public static void createFinRay(Vector3 startFinBase, Vector3 endPoint, int splitNum, out Vector3[] _FINRAY)
		{
			List<Vector3> rayList = new List<Vector3>();

			//開始点と終了点が異なり、基底の数が1以上のとき
			if(startFinBase != endPoint || splitNum > 1){

				float space = 1f / (splitNum - 1);
				Vector3 d = endPoint - startFinBase;
				Vector3 spaced = d * space;

				for (int i=1; i<splitNum; i++){
					Vector3 tmp = startFinBase + spaced * i + adjust * Vector3.right * i*i;
					rayList.Add(tmp);
				}
			}else{
				rayList.Add(endPoint);
				splitNum = 2;
			}

			//鰭条の頂点配列を返す.
			Vector3[] finRay = rayList.ToArray();
			_FINRAY = finRay;

			return;
		}

		public static Vector3 createFinMemberance(Vector3 startFinBase, Vector3 endPoint, int splitNum)
		{

			return Vector3.zero;
		}
	}
}
