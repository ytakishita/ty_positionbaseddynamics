using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class Fin{

		public string Name;

		public FinBase finBase;
		public FinRay finRay;
		public FinMemberance finMemberance;


		public List<Point> finPointList   = new List<Point>();

		public Fin(){
			Name = "Fin";
			finBase = new FinBase();
			finRay = new FinRay();
			finMemberance= new FinMemberance();
		}

		public Fin(string name){
			Name = name;
			finBase = new FinBase();
			finRay = new FinRay();
			finMemberance= new FinMemberance();
		}

		public Fin(FinShapePalam palam){
			Name = palam.name;
			finBase = new FinBase();
			finRay = new FinRay();
			finMemberance= new FinMemberance();
		}

		void setName(string name){
			Name = name;
		}

		public Point getPoint(int index){
			return finPointList[index];

		}

		public int getBaseNum(){
			return finBase._Points.Length;
		}

		public int getRayNum(){
			return finRay.points.Length;
		}

		public int getMemberanceNum(){
			return finMemberance._Points.Length;
		}

		public Point[] getAllPoints(){

			List<Point> pointList = new List<Point>();

			foreach(Point i in finBase._Points){
				pointList.Add(i);
			}
			foreach(Point i in finRay.points){
				pointList.Add(i);
			}
			if(finMemberance._Points != null){
				foreach(Point i in finMemberance._Points){
					pointList.Add(i);
				}
			}
			Point[] points;
			points = pointList.ToArray();

			return points;

		}



		public void setFinBase(FinBase _B){
			finBase = _B;
		}

		public void setFinRay(FinRay _R){
			finRay = _R;
		}

		public void setFinMemberance(FinMemberance _M){
			finMemberance = _M;
		}

		public FinBase getFinBase(){
			return finBase;
		}

		public FinRay getFinRay(){
			return finRay;
		}

		public FinMemberance getFinMemberance(){
			return finMemberance;
		}

		public int CountPointNum(){
			int n=0;
			n += finBase._Points.Length;
			n += finRay.points.Length;
			n += finMemberance._Points.Length;
			return n;
		}


		///デバッグ用-----------------------

		public void LogAllFinPosition(){
			Debug.Log("[ " + this.GetType().FullName + " ] "+ Name );
			int i=0;
			foreach(Point _p in finBase._Points){

				Debug.Log("finBase. "+ i++ + ": "+ _p.p0 );
			}
			i=0;

			foreach(Point _p in finRay.points){

				Debug.Log("finRay. "+ i++ + ": "+  _p.p0 );
			}
			Debug.Log("*****************************");
		}



		public void LogAllFinPoint(){
			Debug.Log("[ " + this.GetType().FullName + " ] "+ Name );
			int i=1;
			foreach(Point _p in finBase._Points){

				Debug.Log("finBase. "+ i++ + ": "+ _p.p0 );
			}
			i=1;
			foreach(Point _p in finRay.points){

				Debug.Log("finRay. "+ i++ + ": "+  _p.p0 );
			}
			i=1;
			foreach(Point _p in finMemberance._Points){

				Debug.Log("finMemberance. "+ i++ + ": "+ _p.p0 );
			}
			Debug.Log("*****************************");
		}
	}
}
