using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class FinRay{

		public int N;
		public int rayNum;
		public int[] rayPointNum;
		public FinBase finBase;
		public Vector3[][] firstPosition;
		public Vector3[][] position;
		public Point[] points;

		public FinRay(){

		}

	 	public FinRay(FinBase _B){
			finBase = _B;
			rayNum = finBase._Points.Length;
			rayPointNum = new int[rayNum];
			position = new Vector3[rayNum][];
		}

		public void FinRaySet(){

			int i=0, j=0;
			foreach(Point p in points){
				position[i][j++] = p.p0;
				if(position[i].Length == j){
					i++;
					j=0;
				}

			}

		}

	}
}
