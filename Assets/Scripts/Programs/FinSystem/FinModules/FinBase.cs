using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class FinBase{

		public Point[] _Points;
		public List<BaseControl> BaseControlList = new List<BaseControl>();
		public List<Point> ControledRayPointList = new List<Point>();

		public struct Range{
			public float angle;
			public float max;
			public float min;
			public void check(){
				if(angle > max){angle = max;}
				if(angle < min){angle = min;}
			}
			public Range(float _Max){
				angle = 0f;
				max = _Max;
				min = 0f;
			}
		}

		public struct BaseControl{
			Point basePoint;
			Point rayPoint;
			public Vector3 axis;
			public float radius;
			public Range Theta;
			public Range Phi;

			public BaseControl(Point _baseP, Point _rayP, float _ThetaMax){
				basePoint = _baseP;
				rayPoint = _rayP;
				axis = _rayP.p0 - _baseP.p0;
				radius = axis.magnitude;
				Theta = new Range(_ThetaMax);
				Phi = new Range(0f);
			}

			public void AngleSet(Point _newRayP){

				Vector3 d = _newRayP.p0 - basePoint.p0;
				Theta.angle = Vector3.Angle(axis, d);
			}

		};


		public void setFinBase(Vector3[] vec){

			List<Point> pointlist = new List<Point>();

			for(int i=0; i<vec.Length; i++){
				Point newPoint = new Point(vec[i]);
				pointlist.Add(newPoint);
			}
			_Points = pointlist.ToArray();
		}

		public Vector3[] getPosition(){
			Vector3[] _V = new Vector3[_Points.Length];
			int i=0;
			foreach(Point p in _Points){
				_V[i++] = p.p0;
			}
			return _V;
		}

		public Point[] getPoints(){
			return _Points;
		}

		public void addBaseController(Point _baseP, Point _rayP, float _ThetaMax){

			if( !ControledRayPointList.Contains(_rayP) ){
				ControledRayPointList.Add(_rayP);
				BaseControlList.Add(new BaseControl(_baseP, _rayP, _ThetaMax));
			}
		}

		public void changeThetaMax(float _ThetaMax){

			for(int i=0; i < BaseControlList.Count; i++) {
				BaseControl _bc = BaseControlList[i];
				_bc.Theta.max = _ThetaMax;
				BaseControlList[i] = _bc;

			}
		}

		public void run(int ns){


		}


	}
}
