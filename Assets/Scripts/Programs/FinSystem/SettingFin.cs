using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{

	//情報を元にFinクラスおよびその中のFinBase, FinRay, FinMemberanceのクラスを設定するクラス。
	//一度入れてしまえば、後はPBDの計算にまわすだけ、のはず
	public static class SettingFin{

		public enum WitchFinSet{
			BASE, RAY, MEMBERANCE
		}

		public static WitchFinSet SetType;
		private static GameObject[] allPoints;

		//MeshからFinを作成
		public static void settingFinFromMesh(out Fin _FIN){

			_FIN = new Fin();
			allPoints = GameObject.FindGameObjectsWithTag("Point");

			PointEditor.FinType Type;
			List<Point> baseList				 = new List<Point>();
			List<Point> rayList 				 = new List<Point>();
			List<Point> memberanceList   = new List<Point>();

			int i=0;
			foreach (GameObject obj in allPoints) {

				Type = obj.GetComponent<PointEditor>().finType;
				Vector3 pos;
				Point _p;

				switch(Type){

					case PointEditor.FinType.BASE:
					pos = obj.transform.position;
					_p = new Point(pos, i++, Point.PointType.BASE);
					baseList.Add(_p);
					_FIN.finPointList.Add(_p);
					break;

					case PointEditor.FinType.RAY:
					pos = obj.transform.position;
					//rayList.Add(new Point(pos, i++, Point.PointType.RAY));
					_p = new Point(pos, i++, Point.PointType.RAY);
					_p.set_m(0.8f);
					rayList.Add(_p);
					_FIN.finPointList.Add(_p);
					break;

					case PointEditor.FinType.MEMBERANCE:
					pos = obj.transform.position;
					//memberanceList.Add(new Point(pos, i++, Point.PointType.MEMBERANCE));
					_p = new Point(pos, i++, Point.PointType.MEMBERANCE);
					_p.set_m(0.2f);
					memberanceList.Add(_p);
					_FIN.finPointList.Add(_p);
					break;

					case PointEditor.FinType.NONE:
					if(FinController.useNoneTypeAsMemberance){
						pos = obj.transform.position;
						//memberanceList.Add(new Point(pos, i++, Point.PointType.MEMBERANCE));
						_p = new Point(pos, i++, Point.PointType.MEMBERANCE);
						_p.set_m(0.2f);
						memberanceList.Add(_p);
						_FIN.finPointList.Add(_p);
					}

					break;
				}
			}

			_FIN.finBase._Points = baseList.ToArray();
			_FIN.finRay.points = rayList.ToArray();
			_FIN.finMemberance._Points = memberanceList.ToArray();
			// _FIN.finPointList.AddRange(baseList);
			// _FIN.finPointList.AddRange(rayList);
			// _FIN.finPointList.AddRange(memberanceList);

			// setFinBase();
			// setFinRay();
			// setFinMemberance();

			return;
		}

		private static void setFinBase(){


			return;
		}

		private static void setFinRay(){


			return;
		}

		private static void setFinMemberance(){

			return;
		}

		public static void clickSetFinBaseToggle(){
			SetType = WitchFinSet.BASE;
		}
		public static void clickSetFinRayToggle(){
			SetType = WitchFinSet.RAY;
		}
		public static void clickSetFinMemberanceToggle(){
			SetType = WitchFinSet.MEMBERANCE;
		}

		//PalameterからFinを作成。
		public static void settingFinFromPalam(FinShapePalam palam, out Fin _FIN){

			_FIN = new Fin(palam);
			List<Point> pointList = new List<Point>();

			//基底をつくる.
			_FIN.finBase.setFinBase( CreateNewFin.createFinBase(
			palam.BaseStartPoint,
			palam.BaseEndPoint,
			ref palam.baseNum,
			palam.stiffness
			) );

			//鰭条をつくる.
			_FIN.finRay.N = _FIN.getBaseNum();
			_FIN.finRay.rayNum = _FIN.getBaseNum();
			_FIN.finRay.rayPointNum = palam.splitNum;
			_FIN.finRay.position = new Vector3[_FIN.getBaseNum()][];
			pointList = new List<Point>();

			Vector3[] basePos = _FIN.finBase.getPosition();
			for(int i=0; i < palam.baseNum; i++){

				Vector3 pos = basePos[i];
				Vector3[] newRay;

				CreateNewFin.createFinRay(
				pos,
				palam.RayEndPoints[i],
				palam.splitNum[i],
				out newRay
				);

				_FIN.finRay.position[i] = newRay;

				foreach(Vector3 list_i in newRay){
					pointList.Add(new Point(list_i));
				}
			}
			_FIN.finRay.points = pointList.ToArray();
		}
	}
}
