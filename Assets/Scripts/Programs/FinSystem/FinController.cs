using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{

	//Finの設定からPBD計算を回すまで親に当たるクラス。
	//ポイントを使うかメッシュを使うかで挙動が異なる。
	public class FinController : MonoBehaviour {

		public bool restart;
		public bool usepoint;
		public bool usemesh;
		public bool useNone;
		public static bool useNoneTypeAsMemberance;

		public int kFinNum;
		public List<Fin> FinList;
		public List<FinShapePalam> CreateFinPalamList;

		//PBD parameter
		public Vector3 gravity;
		public Vector3 extForce;
		public int iteration;

		//生成するひれ ***************
		public FinShapePalam DosalFin;
		public Fin MeshFin;
		public int finIndex;

		private PositionBasedDynamics PBDClass;
		private bool hasFinSetted;


		//*************** *************** ***************

		public Fin getFin(int n){
			return FinList[n];
		}

		public Fin[] getAllFin(){
			return FinList.ToArray();
		}

		// Use this for initialization
		void Start () {

			//Pointを使うかMeshを使うのか真理値
			usepoint = false;
			usemesh  = false;

			Debug.Log("[ " + this.GetType().FullName + " ] StartCheckPoint: 1" );
			//リスタートトリガー
			restart = true;

			hasFinSetted = false;

			//PBDで使用するパラメータ
			extForce	 = new Vector3(0f, 0f, 0f);
			iteration	 = 20;

			//使用するクラス
			FinList									= new List<Fin>();
			CreateFinPalamList			= new List<FinShapePalam>();
			PBDClass 								= GetComponent<PositionBasedDynamics>();
			//Finの数
			kFinNum = 0;

			//Meshを使っているかのチェック
			if(GetComponent<MeshFilter>()){
				usemesh = true;
			}else {
				usepoint = true;
			}

			//*************** *************** ***************

			//PalameterからFinのクラスを作る.
			if(usepoint){

				//Point用のパラメータ設定
				setFinPalam();

				for (int palam_i=0; palam_i<kFinNum; palam_i++){
					Fin tmpFin;
					SettingFin.settingFinFromPalam(CreateFinPalamList[palam_i], out tmpFin);
					FinList.Add(tmpFin);
				}
				hasFinSetted = true;
			}
			if(usemesh){

				MeshFin = new Fin("MeshFin");
				finIndex = FinList.Count;
				FinList.Add(MeshFin);

			}

			//設定したFinのパラメータからPBDの初期化
			if(hasFinSetted){
				PBDClass.InitPBD(FinList[0]);

			}
			//数を入力.
			kFinNum = FinList.Count;
			useNone = true;
			useNoneTypeAsMemberance = true;

			//Finのセット完了

			restart = false;

		}

		public void clickedStartButton(){
			MeshHandler meshHandler = GetComponent<MeshHandler>();
			meshHandler.InitMeshSet();

		}

		public void clickedSetFinButton(){


			Fin tmpFin;
			if(!hasFinSetted){
				SettingFin.settingFinFromMesh(out tmpFin);
				GetComponent<MeshHandler>().setNextPointList(tmpFin);
				//tmpFin.LogAllFinPoint();
				FinList[finIndex] = tmpFin;
				//FinList[finIndex].LogAllFinPoint();
				PBDClass.InitPBD(FinList[finIndex]);
				GetComponent<MeshHandler>().ChangeUpdate();

				hasFinSetted = true;
			}else{


			}

		}


		// Update is called once per frame
		void Update () {

			if(useNone != useNoneTypeAsMemberance){
				useNoneTypeAsMemberance = useNone;
			}

			if(!hasFinSetted){




			}
			else if(hasFinSetted){
				for(int fin_i=0; fin_i < kFinNum; fin_i++){
					Fin tmpFin = FinList[fin_i];

					//PBDの外力設定.
					PBDClass.setGravity(gravity);
					PBDClass.setFext(extForce);
					iteration = Mathf.Max(iteration, 0);
					iteration = Mathf.Min(iteration, 100);
					PBDClass.setIter(iteration);

					//PBD処理
					PBDClass.doPBD(ref tmpFin);

					if(usepoint){
						tmpFin.finRay.FinRaySet();
					}else if(usemesh){
						GetComponent<MeshHandler>().setAllMeshPoint(tmpFin);
					}

					//配列に入れ直す
					FinList[fin_i] = tmpFin;
				}
			}
		}

		private void setFinPalam(){
			//パラメータ設定 ***************
			DosalFin = new FinShapePalam(
			"DosalFin",
			4, 1,
			new Vector3(-2, 1, 0),
			new Vector3(1, 0, 0),
			new Vector3[]{
				new Vector3(-1f, 5f, 0f),
				new Vector3(0f, 4.5f, 0f),
				new Vector3(1f, 4f, 0f),
				new Vector3(2f, 3f, 0f)
			}
			,
			new int[]{ 4, 4, 4, 4 }
			);

			//新しくひれをパラメータで生成する場合、ここに追加 ***************
			CreateFinPalamList.Add(DosalFin);
			kFinNum++;

			return;

		}


		public void setExtForce(Vector3 _f){
			extForce = _f;
		}
	}

	public struct FinShapePalam{

		public string name;
		public int baseNum;
		public int stiffness;
		public Vector3 BaseStartPoint;
		public Vector3 BaseEndPoint;

		public Vector3[] RayEndPoints;
		public int[] splitNum;

		public FinShapePalam(string Name, int n, int s, Vector3 SBP, Vector3 BEP, Vector3[] REP, int[] sp){

			name = Name;
			baseNum = n;
			stiffness = s;
			BaseStartPoint = SBP;
			BaseEndPoint = BEP;
			RayEndPoints = REP;
			splitNum = sp;

		}
	}
}
