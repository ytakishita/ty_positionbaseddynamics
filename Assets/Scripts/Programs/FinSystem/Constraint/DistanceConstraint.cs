using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class DistanceConstraint : Constraint{

		public float distance;
		public static int counter=0;

		public struct DisPair{
			int p1, p2;

			public DisPair(int _p1, int _p2){
				p1 = _p1;
				p2 = _p2;
			}
		}
		static public List<DisPair> finPointList   = new List<DisPair>();

		public DistanceConstraint() {
			//indices = new List<int>();
			distance = 0.0f;
			p = new Point[2];
			p[0] = new Point(new Vector3(0, 0, 0));
			p[1] = new Point(new Vector3(0, 0, 0));
		}

		public DistanceConstraint(Point _P1, Point _P2){

			Vector3 d = _P2.p_def - _P1.p_def;
			this.distance = d.magnitude;

			p = new Point[2];
			p[0] = _P1;
			p[1] = _P2;
		}

		public void run(int ns){
			// p1, p2, w1, w2, d

			float dT = Time.deltaTime;
			Vector3 point1 = p[0].p1;
			Vector3 point2 = p[1].p1;

			float w1 = p[0].w;
			float w2 = p[1].w;

			float d = distance;

			float wSum = w1 + w2;
			float d_p12 = (point1 - point2).magnitude;
			float dSub = d_p12 - d;

			Vector3 dp1 = -1 * (w1 / wSum) * dSub * ((point1 - point2) / d_p12);
			Vector3 dp2 = 1 * (w2 / wSum) * dSub * ((point1 - point2) / d_p12);

			float t = _k (ns) * dT;

			p[0].p1 = point1 + t * dp1;
			p[1].p1 = point2 + t * dp2;

		}
		public float _k (int ns)
		{
			return 1.0f - Mathf.Pow (1.0f - k, 1.0f / ns);
		}

		static public bool CheckAndAdd(int _p1, int _p2){

			DisPair pair;
			if(_p1 < _p2){
				pair = new DisPair(_p1, _p2);
			}else {
				pair = new DisPair(_p2, _p1);
			}

			if(finPointList.Contains(pair)){
				Debug.Log("すでに含まれています( " + _p1 + ", " + _p2 + " )");
				return false;
			}else {
				Debug.Log("ペアを追加しました( " + _p1 + ", " + _p2 + " )");
				finPointList.Add(pair);
				return true;
			}

		}

	}
}
