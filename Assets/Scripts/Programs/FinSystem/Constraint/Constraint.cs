using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
  abstract public class Constraint{
    // cardinality
    public int n;

    // indices
    //1...n
    public List<int> indices;

    //position
    public Point[] p;

    //stiffness  0...1
    public float k = 1.0f;

    // equality or inequality
    //   equality: eval() == 0
    // inequality: eval() >= 0
    public bool isEquality = true;

    public Constraint(){

    }

    public Constraint(/*int[] indices, */Point[] _P) {
      this.n = _P.Length;
      //this.indices = new List<int>(indices);
      this.p = _P;
      //this.p = indices;
    }

    public void run(int ns){

    }

  }
}
