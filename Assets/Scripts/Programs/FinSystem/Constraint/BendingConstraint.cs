using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class BendingConstraint: Constraint{

		public float phiRad;
		public float ToleranceAngle = 30.0f;
		private float angle = -1.0f;
		private Vector3 rotAxis = Vector3.zero;

		public struct BendPairs{
			int p1, p2, p3, p4;

			public BendPairs(int _p1, int _p2, int _p3, int _p4){
				p1 = _p1;
				p2 = _p2;
				p3 = _p3;
				p4 = _p4;
			}
		}
		static public List<BendPairs> finPointList   = new List<BendPairs>();

		public BendingConstraint(Point _P1, Point _P2, Point _P3, Point _P4, float _PhiDeg){

			phiRad = _PhiDeg * Mathf.Deg2Rad;

			p = new Point[4];
			p[0] = _P1;
			p[1] = _P2;
			p[2] = _P3;
			p[3] = _P4;
		}
		public void run(int ns)
		// (int n1, int n2, int n3, int ns)
		{
			Debug.Log("[ " + this.GetType().FullName + " ] Points: " + p[0].index +  ", " + p[1].index + ", " + p[2].index + ", " + p[3].index + " Phi: " + phiRad* Mathf.Rad2Deg);

			float dT = Time.deltaTime;
			float t = _k (ns) * dT;
			//p1, p2, w1, w2, phi
			//float phi = 120f;//standardAngle;

			Vector3 p1 = p[0].p1;
			Vector3 p2 = p[1].p1;
			Vector3 p3 = p[2].p1;
			Vector3 p4 = p[3].p1;

			float w1 = p[0].w;
			float w2 = p[1].w;
			float w3 = p[2].w;
			float w4 = p[3].w;

			float[] w = new float[] {
				w1, w2, w3, w4
			};

			float wSum = w1 + w2 + w3 + w4;
			//*Debug.Log("[ " + this.GetType().FullName + " ]wSum: " + wSum);
			Vector3 edge01 = p1 - p3;
			Vector3 edge02 = p2 - p3;
			Vector3 edge11 = p1 - p4;
			Vector3 edge12 = p2 - p4;

			Vector3 n1 = Vector3.Cross (edge01, edge02);
			Vector3 n2 = Vector3.Cross (edge11, edge12);

			Vector3.Normalize (n1);
			Vector3.Normalize (n2);

			float d = Vector3.Dot (n1, n2);

			Vector3 q3 = (Vector3.Cross(p2, n2) + Vector3.Cross(n1, p2) * d) / (Vector3.Cross(p2, p3)).magnitude;
			Vector3 q4 = (Vector3.Cross(p2, n1) + Vector3.Cross(n2, p2) * d) / (Vector3.Cross(p2, p4)).magnitude;

			Vector3 t1 = (Vector3.Cross(p3, n2) + Vector3.Cross(n1, p3) * d) / (Vector3.Cross(p2, p3)).magnitude;
			Vector3 t2 = (Vector3.Cross(p4, n1) + Vector3.Cross(n2, p4) * d) / (Vector3.Cross(p2, p4)).magnitude;

			Vector3 q2 = -t1 -t2;
			Vector3 q1 = -q2 -q3 -q4;

			float sec1 = Mathf.Sqrt (1 - d*d);
			float sec2 = Mathf.Acos (d) - phiRad;
			float sec3 = w1 * q1.magnitude + w2 * q2.magnitude + w3 * q3.magnitude + w4 * q4.magnitude;

			Vector3 dp1 = (w1 * sec1 * sec2) / sec3 * q1;
			Vector3 dp2 = (w2 * sec1 * sec2) / sec3 * q2;
			Vector3 dp3 = (w3 * sec1 * sec2) / sec3 * q3;
			Vector3 dp4 = (w4 * sec1 * sec2) / sec3 * q4;

			p[0].p1 = p1 + t * dp1;
			p[1].p1 = p2 + t * dp2;
			p[2].p1 = p3 + t * dp3;
			p[3].p1 = p4 + t * dp4;

		}

		//PBDの計算用
		public float _k (int ns)
		{
			return 1.0f - Mathf.Pow (1.0f - k, 1.0f / ns);
		}

		static public bool Add(int _p1, int _p2, int _p3, int _p4){

			BendPairs pair = new BendPairs(_p1, _p2, _p3, _p4);

			if(finPointList.Contains(pair)){
				Debug.Log("すでに含まれています( " + _p1 + ", " + _p2 + " )");
				return false;
			}else {
				Debug.Log("ペアを追加しました( " + _p1 + ", " + _p2 + " )");
				finPointList.Add(pair);
				return true;
			}

		}


	}
}
