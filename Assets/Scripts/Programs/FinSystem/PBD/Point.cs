using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
  public class Point {

    public enum PointType{
  		BASE, RAY, MEMBERANCE, NONE
  	}

    public List<int> nextPointList;

    public int index;
    public Vector3 p_def;
    public Vector3 p0;
    public Vector3 p1;
    public Vector3 v;
    public float m;
    public float w;
    public PointType type;

    public Point(Vector3 _p) {
      nextPointList = new List<int>();
      p_def = _p;
      p0 = _p;
      p1 = _p;
      v = Vector3.zero;
      m = 1.0f;
      w = 1.0f / m;
    }

    public Point(Vector3 _p, int _i, PointType _t) {
      nextPointList = new List<int>();
      index = _i;
      type = _t;
      p_def = _p;
      p0 = _p;
      p1 = _p;
      v = Vector3.zero;
      m = 1.0f;
      w = 1.0f / m;
    }

    public void set_m(float _m){
      m = _m;
      w = 1.0f / _m;
    }
  }
}
