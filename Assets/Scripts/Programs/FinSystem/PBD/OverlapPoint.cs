using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class OverlapPoint : Point{
		public List<int> sameList;

		public OverlapPoint(Vector3 _p): base(_p) {
			sameList = new List<int>();
			p0 = _p;
			p1 = _p;
			v = Vector3.zero;
			m = 1.0f;
		}
	}
}
