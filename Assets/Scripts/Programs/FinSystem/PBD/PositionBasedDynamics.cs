using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FinSystem{
	public class PositionBasedDynamics : MonoBehaviour{

		//外力.
		private static Vector3 gravity;
		private static Vector3 f_ext;

		//屈曲制約
		public float _phi = 120f;

		//繰り返し回数
		public int solverIterations;

		//時間間隔
		private static float dT;

		//PBD計算時に使うPointを入れる配列.
		private Point[] points;

		//制約条件関数.
		private float k = 1.0f;
		private List<DistanceConstraint> DisConstraints;
		private List<BendingConstraint> BendConstraints;
		//private List<> DisConstraints;

		void Start(){

		}

		void Update(){


		}

		//PBD初期化
		public void InitPBD (Fin fin){

			Debug.Log("[ " + this.GetType().FullName + " ] Input Fin" + fin.finBase._Points[0]);

			//制約条件整理用
			DisConstraints = new List<DistanceConstraint>();
			BendConstraints = new List<BendingConstraint>();

			//ここでFinBase, FinRayからPointを取得.
			points = fin.getAllPoints();

			//PBD用パラメータの初期化
			dT = Time.deltaTime;
			solverIterations = 3;
			gravity = Physics.gravity;
			f_ext = Vector3.zero;

			//制約条件の作成(距離制約)

			//Pointの時の初期化
			bool usepoint = GetComponent<FinController>().usepoint;
			bool usemesh = GetComponent<FinController>().usemesh;
			if(usepoint){
				int ray_i = 0;
				for(int j=0; j< fin.getBaseNum(); j++){
					for(int i=0; i<fin.finRay.position[j].Length; i++){

						Point baseP = fin.finBase._Points[j];
						Point rayP = fin.finRay.points[ray_i];

						//Point[] p = fin.finRay.points;
						if(i == 0){
							Debug.Log("[ " + this.GetType().FullName + " ] make base pos");
							DisConstraints.Add(new DistanceConstraint(baseP, rayP));
							Point _P1 = fin.finRay.points[ray_i];
							Point _P2 = fin.finRay.points[ray_i + 1];
							Debug.Log("[ " + this.GetType().FullName + " ] " +  fin.finRay.position[j].Length);

//							Vector3 edge1 = baseP.p1 - _P1.p1;
//							Vector3 edge2 = _P2.p1 - _P1.p1;
//							float angle = Vector3.Angle (edge1, edge2);
//
//							BendConstraints.Add(new BendingConstraint(baseP, _P1, _P2, angle));
//							for(int k=0; k<fin.finRay.position[j].Length - 3; k++){
//								Point _p1 = fin.finRay.points[ray_i + k];
//								Point _p2 = fin.finRay.points[ray_i + k + 1];
//								Point _p3 = fin.finRay.points[ray_i + k + 2];
//								BendConstraints.Add(new BendingConstraint(_p1, _p2, _p3, _phi));
//							}

						}else{
							Debug.Log("[ " + this.GetType().FullName + " ] make ray pos");
							Point rayP1 = fin.finRay.points[ray_i++];
							Point rayP2 = fin.finRay.points[ray_i];
							DisConstraints.Add(new DistanceConstraint(rayP1, rayP2));
						}
					}
					ray_i++;
				}
			}

			//Meshの時の初期化
			else if(usemesh){
				int counter = 1;

				//基底から制約条件を付ける
				foreach(Point _p in fin.finBase._Points){
					foreach(int index in _p.nextPointList){

						Point _pp = fin.finPointList[index];

						//基底から鰭条への、距離制約の付与
						if( _pp.type == Point.PointType.RAY
						&& DistanceConstraint.CheckAndAdd(_p.index, _pp.index))
						{

							if(_p.index <  _pp.index){
								DisConstraints.Add(new DistanceConstraint(_p, _pp));
							}else{
								DisConstraints.Add(new DistanceConstraint(_pp, _p));
							}

							Vector3 d = _pp.p_def - _p.p_def;
							float distance = d.magnitude;
							Debug.Log("[ " + this.GetType().FullName + " ] Points: " + counter++ +" ( " + _p.index + ", " + _pp.index + " ) = "  +_p.p_def + ", " + _pp.p_def);
							Debug.Log("[ " + this.GetType().FullName + " ] distance: " + distance);

						}

						//基底コントローラーの付与
						if( _pp.type == Point.PointType.RAY){
							fin.finBase.addBaseController(_p, _pp, 30.0f);

						}


						//屈曲制約の付与
						if( _pp.type == Point.PointType.RAY){

							//基底からつながっている鰭条ひとつと、その鰭条からつながっている別の鰭条点と屈曲制約を付ける
							foreach(int index2 in _pp.nextPointList){
								Point _ppp = fin.finPointList[index2];
								if( _ppp.type == Point.PointType.RAY){

									Vector3 edge1 = _p.p1 - _pp.p1;
									Vector3 edge2 = _ppp.p1 - _pp.p1;
									float angle = Vector3.Angle (edge1, edge2);

									//BendConstraints.Add(new BendingConstraint(_p, _pp, _ppp, angle));
									Debug.Log("[ " + this.GetType().FullName + " ] BendPoints: " + " ( " + _p.index + ", " + _pp.index + ", " + _ppp.index + ")");
								}
							}
						}
					}
				}

				//鰭条から鰭条への距離制約の付与
				foreach(Point _p in fin.finRay.points){

					//鰭条への隣接頂点を検索して重複していなければ追加
					foreach(int index in _p.nextPointList){

						Point _pp = fin.finPointList[index];

						if(_pp.type == Point.PointType.RAY
						&& DistanceConstraint.CheckAndAdd(_p.index, _pp.index))
						{
							if(_p.index <  _pp.index){
								DisConstraints.Add(new DistanceConstraint(_p, _pp));
							}else{
								DisConstraints.Add(new DistanceConstraint(_pp, _p));
							}

							Vector3 d = _pp.p_def - _p.p_def;
							float distance = d.magnitude;
							Debug.Log("[ " + this.GetType().FullName + " ] Points: " + counter++ +" ( " + _p.index + ", " + _pp.index + " ) = "  +_p.p_def + ", " + _pp.p_def);
							Debug.Log("[ " + this.GetType().FullName + " ] distance: " + distance);
						}
					}
				}

				//鰭膜から他の点への距離制約
				foreach(Point _p in fin.finMemberance._Points){

					//隣接頂点全体を検索して重複していなければ追加
					foreach(int index in _p.nextPointList){

						Point _pp = fin.finPointList[index];
						if(_pp.type != Point.PointType.NONE
						&& DistanceConstraint.CheckAndAdd(_p.index, _pp.index))
						{
							if(_p.index <  _pp.index){
								DisConstraints.Add(new DistanceConstraint(_p, _pp));
							}else{
								DisConstraints.Add(new DistanceConstraint(_pp, _p));
							}

							Vector3 d = _pp.p_def - _p.p_def;
							float distance = d.magnitude;
							Debug.Log("[ " + this.GetType().FullName + " ] Points: " + counter++ +" ( " + _p.index + ", " + _pp.index + " ) = "  +_p.p_def + ", " + _pp.p_def);
							Debug.Log("[ " + this.GetType().FullName + " ] distance: " + distance);

						}
					}
				}
			}
		}

		//PBDを実行
		public void doPBD (ref Fin Fin)
		{

			dT = Time.deltaTime;

			//------------PBD計算------------

			//		(4)ーループ始め
			//		→Update自体がループ

			//		(5)全ての頂点の速度に外力を加える
			//外力計算
			foreach (Point point in points) {
				//v[i] = v[i] + dT * w[i] * f_ext[i];
				point.v = point.v + dT * point.w * f_ext;
				//v [i] += g * dT;
				point.v += gravity * dT;
			}

			foreach (Point point in Fin.finBase._Points){



			}

			//		(6)速度に減衰計算を行う
			//		→やらない

			//		(7)速度と経過時間から理想位置を算出
			foreach (Point point in points) {
				point.p1 = point.p0 + dT * point.v;
			}

			//		(8)衝突の制約条件を加えて理想位置を更新
			//		→やらない

			//		(9)ーループ始め〜繰り返し回数だけ施行
			for (int solver_i = 0; solver_i < solverIterations; solver_i++) {

				//		(10)制約条件を計算
				//projectConstraint(C, p);
				//[距離制約 distance constraint]

				//制約条件の適用

				//基底位置の正常化
				Fin.finBase.run(solver_i);


				//屈曲制約の実行
				for(int _i= 0; _i <BendConstraints.Count(); _i++){
					//BendConstraints[_i].run(solver_i);
				}

				//距離制約の実行
				for(int _i= 0; _i <DisConstraints.Count(); _i++){
					//DistanceConstraint D = constraints[_i];
					//D.run(solver_i);
					DisConstraints[_i].run(solver_i);
				}
				//		(11)ーループ終わり
			}

			//基底は動かないようにする.
			foreach(Point p in Fin.finBase._Points){

				p.p1 = p.p0;

			}

			//		(12)ーループ始め〜全ての頂点に対して
			foreach (Point point in points) {
				//		(13)理想位置と現在位置の差分から、速度を計算
				//v [i] = (p [i] - x [i]) / dT;
				point.v = (point.p1 - point.p0) / dT;

				//		(14)理想位置を現在位置に代入
				//x [i] = p [i];
				point.p0 = point.p1;

				//		(15)ーループ終わり
			}


			//		(16)摩擦と反発係数に応じて速度を更新
			//		→やらない
			//velocityUpdate(v);

			//		(17)ーループ終わり


			//------------Unityのオブジェクトに反映------------

			//FinBaseとFinRayのPointを入れた順に入れ直す
			int i = 0;
			for(; i < Fin.getBaseNum(); i++){
				Fin.finBase._Points[i] = points[i];
			}
			for(int i2 = 0;i < Fin.getBaseNum() + Fin.finRay.points.Length; i++, i2++){
				Fin.finRay.points[i2] = points[i];
			}
			for(int i3 = 0;i < Fin.getBaseNum() + Fin.finRay.points.Length; i++, i3++){
				Fin.finMemberance._Points[i3] = points[i];
			}
		}

		//重力gの外部変更用
		public void setGravity(Vector3 _g){
			gravity = _g;
		}

		//外力f_extの外部変更用
		public void setFext(Vector3 _f){
			f_ext = _f;
		}

		//繰り返し回数solverIterationsの外部変更用
		public void setIter(int _i){
			solverIterations = _i;
		}

		//距離制約関数（別クラスがある）

		void distanceConstraint (Point n1, Point n2, int ns, float D)
		{

			// p1, p2, w1, w2, d

			Vector3 point1 = n1.p1;
			Vector3 point2 = n2.p1;

			float w1 = n1.w;
			float w2 = n2.w;

			float d;
			if (D == 0f) {
				d =  2f;//standardLength.magnitude;
			} else {
				d = D;
			}

			float wSum = w1 + w2;
			float d_p12 = (point1 - point2).magnitude;
			float dSub = d_p12 - d;

			Vector3 dp1 = -1 * (w1 / wSum) * dSub * ((point1 - point2) / d_p12);
			Vector3 dp2 = 1 * (w2 / wSum) * dSub * ((point1 - point2) / d_p12);

			float t = _k (ns) * dT;

			n1.p1 = point1 + t * dp1;
			n2.p1 = point2 + t * dp2;
		}

		//屈曲制約関数
		void bendingConstraint (int n1, int n2, int n3, int ns)
		{

			//p1, p2, w1, w2, phi
			float phi = 120f;//standardAngle;

			Vector3 p1 = points [n1].p1;
			Vector3 p2 = points [n2].p1;
			Vector3 p3 = points [n3].p1;

			Vector3 edge1 = p1 - p2;
			Vector3 edge2 = p3 - p2;

			float angle = Vector3.Angle (edge1, edge2);

			if (angle > phi) {
				return;
			}
			//float aSub = angle - phi;
			Vector3 rotAxis = Vector3.Cross (edge1, edge2);
			Quaternion rot = Quaternion.AngleAxis (angle, rotAxis);
			Matrix4x4	rotMat = Matrix4x4.TRS (p2, rot, Vector3.one);
			Vector3 dp3 = rotMat.MultiplyPoint3x4 (edge2) + p2;

			float t = _k (ns) * dT;

			points [n3].p1 = p3 + t * dp3;
			Debug.Log (n2 + " : " + angle);

		}

		//PBDの計算用
		public float _k (int ns)
		{
			return 1.0f - Mathf.Pow (1.0f - k, 1.0f / ns);
		}
	}
}
