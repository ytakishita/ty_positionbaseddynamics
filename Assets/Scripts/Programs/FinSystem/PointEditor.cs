using UnityEngine;
using System.Collections;

public class PointEditor : MonoBehaviour {

	public int index;
	public bool Drag;

	public enum FinType{
		BASE, RAY, MEMBERANCE, NONE
	}

	public  FinType finType;
	private Toggle.WitchFinSet InputType;

	public Material DefaultMaterial;
	public Material FinBaseMaterial;
	public Material FinRayMaterial;
	public Material FinMemberanceMaterial;

	// Use this for initialization
	void Start () {
		Drag = false;
		finType = FinType.NONE;
		DefaultMaterial = GetComponent<MeshRenderer>().material;

	}

	// Update is called once per frame
	void Update () {


	}

	void OnMouseDown() {
		InputType = Toggle.SetType;

		switch(InputType){

			case Toggle.WitchFinSet.BASE:
			finType = FinType.BASE;
			GetComponent<MeshRenderer>().material = FinBaseMaterial;
			break;

			case Toggle.WitchFinSet.RAY:
			finType = FinType.RAY;
			GetComponent<MeshRenderer>().material = FinRayMaterial;
			break;

			case Toggle.WitchFinSet.MEMBERANCE:
			finType = FinType.MEMBERANCE;
			GetComponent<MeshRenderer>().material = FinMemberanceMaterial;
			break;

			case Toggle.WitchFinSet.NONE:
			finType = FinType.NONE;
			GetComponent<MeshRenderer>().material = DefaultMaterial;
			break;
		}


		// WitchFinSet SetType =
		//
		// 	print("MouseDown!");
	}

	void OnMouseDrag(){

		if(Drag){
			Vector3 objectPointInScreen = Camera.main.WorldToScreenPoint(this.transform.position);

			Vector3 mousePointInScreen = new  Vector3(Input.mousePosition.x,
			Input.mousePosition.y,
			objectPointInScreen.z);

			Vector3 mousePointInWorld = Camera.main.ScreenToWorldPoint(mousePointInScreen);
			mousePointInWorld.z = this.transform.position.z;
			this.transform.position = mousePointInWorld;
		}
	}
}
