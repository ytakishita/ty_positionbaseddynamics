using UnityEngine;
using System.Collections;

namespace FinSystem{
	public class GetPoint : MonoBehaviour {

		public GameObject Prefab;
		private Vector3 clickPosition;

		// Use this for initialization
		void Start () {

		}

		// Update is called once per frame
		void Update () {
			if (Input.GetMouseButtonDown(0)) {
				// ここでの注意点は座標の引数にVector2を渡すのではなく、Vector3を渡すことである。
				// Vector3でマウスがクリックした位置座標を取得する
				clickPosition = Input.mousePosition;
				// Z軸修正
				clickPosition.z = 10f;
				// オブジェクト生成 : オブジェクト(GameObject), 位置(Vector3), 角度(Quaternion)
				// ScreenToWorldPoint(位置(Vector3))：スクリーン座標をワールド座標に変換する
				Instantiate(Prefab, Camera.main.ScreenToWorldPoint(clickPosition), Prefab.transform.rotation);
			}

		}

		void findPointInMesh(Vector3 _clickpoint, int index){
			Mesh mesh = GetComponent<MeshFilter>().mesh;

			index = 1;

			return;
		}

	}
}
