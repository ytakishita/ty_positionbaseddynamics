using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FinSystem{
	public class MeshHandler : MonoBehaviour {

		//Meshの値保存用
		private MeshFilter	 meshFilter;
		//private MeshRenderer meshRenderer;
		private Mesh mesh;
		public Vector3[] meshPoints;

		//頂点オブジェクト用
		public GameObject Prefab;
		private GameObject[] allParticle;
		public GameObject Parent;

		//実行用
		private bool update;
		private bool PBDupdate;

		// Use this for initialization
		//メッシュの頂点情報を取得し、その位置に頂点オブジェクトを作成する。
		//この頂点オブジェクトが頂点の操作ボタンになる。
		//頂点オブジェクトをまとめて扱うため、親オブジェクトParentObjectを用意し、その子要素にする。
		//頂点情報の元になるメッシュとサイズや回転を合わせるため、初期位置を手元に保存しておく。

		void Start () {

			update = false;
			PBDupdate = false;
		}

		public void InitMeshSet(){

			meshFilter   = GetComponent<MeshFilter>();
			mesh = meshFilter.mesh;
			meshPoints = mesh.vertices;

			if(Parent == null){
				Parent = GameObject.Find ("ParentObject");
			}

			CreateVerticesObjects();
			Parent.transform.position = this.transform.position;
			Parent.transform.rotation = this.transform.rotation;
			Parent.transform.localScale = this.transform.localScale;

			update = true;
		}

		//親のオブジェクトtransformを初期値にする
		//→頂点位置をセット
		//→メッシュのtransformの値をコピー
		//という流れ
		// Update is called once per frame
		void Update () {

			if(update){
				UpdateMeshSet();
			}else if(PBDupdate){
				PBDUpdate();
			}
		}

		public void UpdateMeshSet(){
			mesh.vertices = meshPoints;
			meshFilter.mesh = mesh;
			setVerticesPoints();
		}

		public void PBDUpdate(){
			setVerticesObjects();
			mesh.vertices = meshPoints;
			meshFilter.mesh = mesh;
		}

		public void StopUpdate(){

			update = false;
			PBDupdate = false;

		}

		public void ChangeUpdate(){

			update = false;
			PBDupdate = true;

		}

		//メッシュ情報を取得
		Mesh getMesh(){
			return mesh;
		}

		//メッシュの頂点から頂点オブジェクトを作成する
		void CreateVerticesObjects(){

			//一旦フィールドにあるPointタグ付きのオブジェクトを削除する
			allParticle = GameObject.FindGameObjectsWithTag("Point");
			foreach (GameObject obj in allParticle) {
				Destroy (obj);
			}

			//タグを付けてオブジェクトを作成
			int i=0;
			foreach(Vector3 _P in meshPoints){
				var obj = Instantiate(Prefab);
				obj.transform.parent = Parent.transform;
				obj.transform.localPosition = _P;

				obj.tag = "Point";
				obj.name = Prefab.name + ":" + i;

				obj.GetComponent<PointEditor>().index = i;
				i++;

			}
			//Parent.transform.rotation = this.transform.rotation;
		}

		//頂点オブジェクトの位置からメッシュの頂点をセット
		void setVerticesPoints(){

			allParticle = GameObject.FindGameObjectsWithTag("Point");
			int i=0;

			foreach(var obj in allParticle){
				meshPoints[i++] = obj.transform.localPosition;
			}
		}

		//Meshの頂点位置に、Objectを合わせる
		public void setVerticesObjects(){

			allParticle = GameObject.FindGameObjectsWithTag("Point");

			int i=0;
			foreach(Vector3 _P in meshPoints){
				var obj = allParticle[i++];
				obj.transform.localPosition = _P;
			}

		}

		//インデックスと位置を入れるとメッシュの頂点の値を変えられる
		void setMeshPoint(int _index, Vector3 _position){

			meshPoints[_index] = _position;
		}

		public void setAllMeshPoint(Fin _FIN){

			Matrix4x4 WToLMat = this.transform.worldToLocalMatrix;

			int n = _FIN.CountPointNum();
			//Debug.Log("[ " + this.GetType().FullName + " ] "+ n );
			Vector3[] allPoints = new Vector3[n];
			foreach(Point _p in _FIN.finBase._Points){
				allPoints[_p.index] = WToLMat.MultiplyPoint3x4(_p.p0);
			}
			foreach(Point _p in _FIN.finRay.points){
				allPoints[_p.index] = WToLMat.MultiplyPoint3x4(_p.p0);
			}
			foreach(Point _p in _FIN.finMemberance._Points){
				allPoints[_p.index] = WToLMat.MultiplyPoint3x4(_p.p0);
			}
			meshPoints = allPoints;

		}

		public void setNextPointList(Fin _Fin){

			List<Point> allPoints = new List<Point>();


			foreach(Point _p in _Fin.finBase._Points){

				int index = _p.index;
				for(int i=0; i < mesh.triangles.Length; i+=3){
					int _1 = mesh.triangles[i];
					int _2 = mesh.triangles[i+1];
					int _3 = mesh.triangles[i+2];

					if(index == _1){
						if(!_p.nextPointList.Contains(_2)){
							_p.nextPointList.Add(_2);
						}
						if(!_p.nextPointList.Contains(_3)){
							_p.nextPointList.Add(_3);
						}
					}
					else if(index == _2){
						if(!_p.nextPointList.Contains(_1)){
							_p.nextPointList.Add(_1);
						}
						if(!_p.nextPointList.Contains(_3)){
							_p.nextPointList.Add(_3);
						}
					}
					else if(index == _3){
						if(!_p.nextPointList.Contains(_2)){
							_p.nextPointList.Add(_2);
						}
						if(!_p.nextPointList.Contains(_1)){
							_p.nextPointList.Add(_1);
						}
					}
					_p.nextPointList.Sort();

				}
			}

			foreach(Point _p in _Fin.finRay.points){

				int index = _p.index;
				for(int i=0; i < mesh.triangles.Length; i+=3){
					int _1 = mesh.triangles[i];
					int _2 = mesh.triangles[i+1];
					int _3 = mesh.triangles[i+2];

					if(index == _1){
						if(!_p.nextPointList.Contains(_2)){
							_p.nextPointList.Add(_2);
						}
						if(!_p.nextPointList.Contains(_3)){
							_p.nextPointList.Add(_3);
						}
					}
					else if(index == _2){
						if(!_p.nextPointList.Contains(_1)){
							_p.nextPointList.Add(_1);
						}
						if(!_p.nextPointList.Contains(_3)){
							_p.nextPointList.Add(_3);
						}
					}
					else if(index == _3){
						if(!_p.nextPointList.Contains(_2)){
							_p.nextPointList.Add(_2);
						}
						if(!_p.nextPointList.Contains(_1)){
							_p.nextPointList.Add(_1);
						}
					}
					_p.nextPointList.Sort();

				}
			}

			foreach(Point _p in _Fin.finMemberance._Points){

				int index = _p.index;
				for(int i=0; i < mesh.triangles.Length; i+=3){
					int _1 = mesh.triangles[i];
					int _2 = mesh.triangles[i+1];
					int _3 = mesh.triangles[i+2];

					if(index == _1){
						if(!_p.nextPointList.Contains(_2)){
							_p.nextPointList.Add(_2);
						}
						if(!_p.nextPointList.Contains(_3)){
							_p.nextPointList.Add(_3);
						}
					}
					else if(index == _2){
						if(!_p.nextPointList.Contains(_1)){
							_p.nextPointList.Add(_1);
						}
						if(!_p.nextPointList.Contains(_3)){
							_p.nextPointList.Add(_3);
						}
					}
					else if(index == _3){
						if(!_p.nextPointList.Contains(_2)){
							_p.nextPointList.Add(_2);
						}
						if(!_p.nextPointList.Contains(_1)){
							_p.nextPointList.Add(_1);
						}
					}
					_p.nextPointList.Sort();

				}
			}
		}


		void getMeshPointFromPosition(Vector3 _position, out int index){
			foreach(Vector3 _pos in meshPoints){
			}
			index = 1;
		}
	}
}
