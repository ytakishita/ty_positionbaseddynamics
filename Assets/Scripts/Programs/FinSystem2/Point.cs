﻿using UnityEngine;
using System.Collections;

namespace FinSystem2{
	public class Point{
		protected Vector3 x;
		protected Vector3 p;
		protected Vector3 v;
		protected float m;
		protected float w;

		public Point(Vector3 _p) {
			x = _p;
			p = _p;
			v = Vector3.zero;
			m = 1.0f;
			w = 1.0f / m;
		}

	}
}
