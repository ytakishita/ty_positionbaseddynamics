using UnityEngine;
using System.Collections;

namespace FinSystem2{

	enum FinComp{
		BASE, RAY, MEMBERANCE
	}

	public class FinPoint : Point{

		//private int COMP;

		public FinPoint(Vector3 _p, int _COMP): base(_p) {
			x = _p;
			p = _p;
			v = Vector3.zero;
			m = 1.0f;
			w = 1.0f / m;
			//COMP = _COMP;
		}

	}
}
