﻿using UnityEngine;
using System.Collections.Generic;

namespace PBDSample{
class PointRenderer : MonoBehaviour
{
	public GameObject prefab = null;
	Dictionary<Point, GameObject> objects;

	public void Start()
	{
		// GameObjectを生産
		objects = new Dictionary<Point, GameObject>();
		foreach (var point in GetComponent<PBD>().points)
		{
			var obj = Instantiate(prefab);
			obj.transform.SetParent(transform);
			objects.Add(point, obj);
		}
	}

	void Update()
	{
		// GameObjectの位置を更新する
		foreach (var obj in objects)
		{
			obj.Value.transform.position = obj.Key.Position;
		}
	}
}
}
