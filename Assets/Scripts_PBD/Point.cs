
﻿using UnityEngine;


namespace PBDSample{
public class Point
{
	public Vector3 Position { get; set; }
	public Vector3 PrevPosition { get; set; }

	public Point()
	{
		Position = PrevPosition = Vector2.zero;
	}
}
}
