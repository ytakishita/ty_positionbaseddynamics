﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace PBDSample{
public class PBD : MonoBehaviour {
	public List<Point> points;

	void Awake () {
		// pointを生産する
		points = new List<Point>();

		foreach (var x in Enumerable.Range(-2, 4))
		{
			foreach (var y in Enumerable.Range(-2, 8))
			{
				foreach (var z in Enumerable.Range(-2, 4))
				{
					var point = new Point();
					point.PrevPosition = point.Position = new Vector3(x + y * 0.1f, y, z + y * 0.1f) * 1.0f;
					points.Add(point);
				}
			}
		}
	}

	void FixedUpdate ()
	{
		foreach (var point in points)
		{
			// 前回との位置の差から速度を求める
			var velocity = (point.Position - point.PrevPosition) / Time.deltaTime;
			velocity += Physics.gravity * Time.deltaTime;

			// 理想位置を更新する
			point.PrevPosition = point.Position;
			point.Position += velocity * Time.deltaTime;
		}


		foreach (var i in Enumerable.Range(0, 3))
		{
			Debug.Log("繰り返し回数 " + i);
			foreach (var point in points)
			{
				Debug.Log(i);

				// 位置を範囲内に制限する
				var position = point.Position;
				position.x = Mathf.Clamp(position.x, -5, 5);
				position.y = Mathf.Clamp(position.y, -5, 5);
				position.z = Mathf.Clamp(position.z, -5, 5);

				point.Position = position;

				// point同士が衝突していたら離す
				foreach (var point2 in points)
				{
					if (point == point2) continue;
					var vec = point2.Position - point.Position;
					if (vec.magnitude < 1)
					{
						point.Position -= vec.normalized * (1 - vec.magnitude) / 2;
						point2.Position += vec.normalized * (1 - vec.magnitude) / 2;
					}
				}
			}
		}
	}
}
}
